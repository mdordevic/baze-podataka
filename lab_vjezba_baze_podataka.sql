Drop table upisi;
Drop sequence UPISI_ID_SEQ;
Drop table studenti;
drop sequence STUDENTI_ID_SEQ;
drop table predmeti;
drop sequence PREDMETI_ID_SEQ;
drop table nastavnici;
drop sequence NASTAVNICI_ID_SEQ;


CREATE TABLE Studenti (
	ID NUMBER(9, 0) NOT NULL,
	JMBAG NUMBER(8, 0) UNIQUE NOT NULL,
	Ime VARCHAR2(20) NOT NULL,
	Prezime VARCHAR2(20) NOT NULL,
	email VARCHAR2(20) UNIQUE NOT NULL,
	password VARCHAR2(10) UNIQUE NOT NULL,
	Spol NUMBER(1, 0) NOT NULL,
	constraint STUDENTI_PK PRIMARY KEY (ID));

CREATE sequence STUDENTI_ID_SEQ;

CREATE trigger BI_STUDENTI_ID
  before insert on Studenti
  for each row
begin
  select STUDENTI_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
CREATE TABLE Nastavnici (
	ID NUMBER(9, 0) NOT NULL,
	Ime VARCHAR2(20) NOT NULL,
	Prezime VARCHAR2(20) NOT NULL,
	email VARCHAR2(30) UNIQUE NOT NULL,
	password VARCHAR2(20) UNIQUE NOT NULL,
	spol NUMBER(1, 0) NOT NULL,
	constraint NASTAVNICI_PK PRIMARY KEY (ID));

CREATE sequence NASTAVNICI_ID_SEQ;

CREATE trigger BI_NASTAVNICI_ID
  before insert on Nastavnici
  for each row
begin
  select NASTAVNICI_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
CREATE TABLE Predmeti (
	ID NUMBER(9, 0) NOT NULL,
	IDnastavnik NUMBER(9, 0) NOT NULL,
	sifra VARCHAR2(10) UNIQUE NOT NULL,
	naziv VARCHAR2(30) NOT NULL,
	ects NUMBER(1, 0) NOT NULL,
	semestar NUMBER(1, 0) NOT NULL,
	constraint PREDMETI_PK PRIMARY KEY (ID));
/
CREATE sequence PREDMETI_ID_SEQ;

CREATE OR REPLACE trigger BI_PREDMETI_ID
  before insert on Predmeti
  for each row
begin
  select PREDMETI_ID_SEQ.nextval into :NEW.ID from dual;
end;
/

CREATE TABLE Upisi (
	ID NUMBER(9, 0) NOT NULL,
	IDpredmeti NUMBER(9, 0) NOT NULL,
	IDstudenti NUMBER(9, 0) NOT NULL,
	datum DATE NOT NULL,
	ocjena NUMBER(1, 0) NOT NULL,
	constraint UPISI_PK PRIMARY KEY (ID));

CREATE sequence UPISI_ID_SEQ;

CREATE trigger BI_UPISI_ID
  before insert on Upisi
  for each row
begin
  select UPISI_ID_SEQ.nextval into :NEW.ID from dual;
end;

/


ALTER TABLE Predmeti ADD CONSTRAINT Predmeti_fk0 FOREIGN KEY (IDnastavnik) REFERENCES Nastavnici(ID);

ALTER TABLE Upisi ADD CONSTRAINT Upisi_fk0 FOREIGN KEY (IDpredmeti) REFERENCES Predmeti(ID);
ALTER TABLE Upisi ADD CONSTRAINT Upisi_fk1 FOREIGN KEY (IDstudenti) REFERENCES Studenti(ID);
ALTER TABLE Studenti ADD CONSTRAINT studenti_spol check (spol between 0 and 1) ;
ALTER TABLE Nastavnici ADD CONSTRAINT nastavnici_spol check (spol between 0 and 1) ;
ALTER TABLE Predmeti ADD CONSTRAINT predmeti_semestar check (semestar between 1 and 6) ;
ALTER TABLE Upisi ADD CONSTRAINT upisi_ocjena check (ocjena between 1 and 5) ;

INSERT into studenti (jmbag, ime, prezime, email, password, spol) values ('41285799','Antonio','Djordjevic','adjordjevic@vub.hr','2534','0');
INSERT into studenti (jmbag, ime, prezime, email, password, spol) values ('12285796','Ivica','Ivanic','iivanic@vub.hr','12345','0');
INSERT into studenti (jmbag, ime, prezime, email, password, spol) values ('121285596','Marko','Maric','mmaric@vub.hr','7444','0');
INSERT into studenti (jmbag, ime, prezime, email, password, spol) values ('11285006','Marin','Marusic','mamaric11@vub.hr','5673','0');
INSERT into studenti (jmbag, ime, prezime, email, password, spol) values ('21281236','Marijan','Horvat','mhorvat@vub.hr','02020','0');
INSERT into studenti (jmbag, ime, prezime, email, password, spol) values ('31245796','Hrvoje','Horvat','hhorvat@vub.hr','12455','0');
INSERT into studenti (jmbag, ime, prezime, email, password, spol) values ('51242378','Milica','Prezime','mprezime@vub.hr','21734','1');
INSERT into studenti (jmbag, ime, prezime, email, password, spol) values ('61273823','Jala','Brat','jbrat@vub.hr','23445','0');
INSERT into studenti (jmbag, ime, prezime, email, password, spol) values ('71205947','Buba','Coreli','bcoreli@vub.hr','21104','0');
INSERT into studenti (jmbag, ime, prezime, email, password, spol) values ('81200005','Marija','Sveta','msveta@vub.hr','04985','1');

INSERT into nastavnici (ime, prezime, email, password, spol) values ('Petar','Peric','m.maric@vub.hr','123','0');
INSERT into nastavnici (ime, prezime, email, password, spol) values ('Ivana','Pranjic','i.ivicic@vub.hr','112','1');
INSERT into nastavnici (ime, prezime, email, password, spol) values ('Slobodan','Stjepancic','m.maric22@vub.hr','92','0');

INSERT into predmeti (IDnastavnik, sifra, naziv, ects, semestar) values (1,'123', 'Engleski', 1, 3);
INSERT into predmeti (IDnastavnik, sifra, naziv, ects, semestar) values (2,'112', 'Njemacki', 2, 2);
INSERT into predmeti (IDnastavnik, sifra, naziv, ects, semestar) values (3,'92', 'OOP', 6, 4);

INSERT into UPISI (IDpredmeta,IDstudenta,datum,ocjena) values (1,1,sysdate-12,5);
INSERT into UPISI (IDpredmeta,IDstudenta,datum,ocjena) values (2,1,sysdate-13,4);
INSERT into UPISI (IDpredmeta,IDstudenta,datum,ocjena) values (2,4,sysdate-24,3);
INSERT into UPISI (IDpredmeta,IDstudenta,datum,ocjena) values (3,3,sysdate-24,1);
INSERT into UPISI (IDpredmeta,IDstudenta,datum,ocjena) values (2,8,sysdate-35,5);